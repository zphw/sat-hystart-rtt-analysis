import matplotlib.pyplot as plt
import pandas as pd
from log import logger


class Plot:
    def __init__(self, csv_file, exit_time):
        self.df = pd.read_csv(csv_file)
        self.exit_time = exit_time
        self.start_time = 0

        self.rtt = []
        self.rtt_diff = []
        self.seconds = []
        self.exit_round_found = False

    def _compute_rtt(self):
        self.start_time = min(self.df['frame.time_epoch'])

        for t in range(len(self.df['frame.time_epoch'])):
            if not pd.isnull(self.df['tcp.analysis.ack_rtt'][t]) and self.df['tcp.dstport'][t] == 12222:
                self.rtt.append(self.df['tcp.analysis.ack_rtt'][t])
                self.seconds.append(self.df['frame.time_epoch'][t] - self.start_time)

                if self.df['frame.time_epoch'][t] > self.exit_time and not self.exit_round_found:
                    logger.info("Slow start exited after {} RTTs".format(len(self.rtt) - 1))
                    self.exit_round_found = True

        self.seconds, self.rtt = (list(t) for t in zip(*sorted(zip(self.seconds, self.rtt))))
        self.rtt_diff.append(0)
        for i in range(1, len(self.rtt)):
            self.rtt_diff.append((self.rtt[i] - self.rtt[i-1]) * 1000)

    def plot(self):
        self._compute_rtt()

        plt.rcParams['font.size'] += 1
        fig, axs = plt.subplots(2)
        fig.set_figheight(8)

        axs[0].scatter(self.seconds, self.rtt, color='tab:blue')
        axs[1].scatter(self.seconds, self.rtt_diff, color='tab:blue')

        axs[0].axvline(x=self.exit_time - self.start_time, color='red')
        axs[1].axvline(x=self.exit_time - self.start_time, color='red')
        axs[0].set_ylabel("RTT (s)")
        axs[1].set_ylabel("RTT Difference (ms)")
        axs[1].set_ylim([0, 100])
        # axs[1].set_yticks([20])
        axs[1].set_xlabel("Time (seconds)")

        axs[0].set_xlim([0, 10])
        axs[1].set_xlim([0, 10])

        plot_filename = f'graphs/rtt-analysis.png'
        plt.savefig(plot_filename)
        logger.info("Plot saved to " + plot_filename)

        plt.show()


def main():
    p = Plot("csv_files/cubic-1625499463.csv", 1625499472.810485)
    p.plot()


if __name__ == '__main__':
    main()
