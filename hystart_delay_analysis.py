import re
import sys
import threading
import time
from log import logger
from plot_rtt_analysis import Plot
from ssh_client import SSHClientOverProxy
from trial import Trial

exit_time = 0


def monitor_thread(client):
    logger.info("Started monitoring network statistics.")

    train_detect_value = 0
    delay_detect_value = 0
    global exit_time
    while True:
        command = "date +%s.%N && netstat --statistics"
        stdin, stdout, stderr = client.exec_command(command)
        output = ""
        is_first = True
        for line in stdout.readlines():
            if is_first:
                exit_time = float(line.replace("\n", ""))
                is_first = False
            output += line

        match = re.search(r"TCPHystartTrainDetect:\s(.*)", output)
        if train_detect_value == 0:
            train_detect_value = match.group(1)
        else:
            if match.group(1) > train_detect_value:
                logger.info("Train detect triggered.")
                sys.exit()

        match = re.search(r"TCPHystartDelayDetect:\s(.*)", output)
        if delay_detect_value == 0:
            delay_detect_value = match.group(1)
        else:
            if match.group(1) > delay_detect_value:
                logger.info("Delay detect triggered.")
                sys.exit()


def main():
    username = ""
    password = ""
    protocol = "cubic"

    trial = Trial(protocol, username, password, iperf_time=10)

    mlcneta_ssh = SSHClientOverProxy(username=username,
                                     password=password,
                                     proxy_host="cs.wpi.edu",
                                     host="mlcneta.cs.wpi.edu")
    monitor_client = mlcneta_ssh.get_client()

    monitor_t = threading.Thread(target=monitor_thread, args=(monitor_client,))
    monitor_t.start()

    trial.run()

    logger.info("Slow start exit time: {}".format(exit_time))

    p = Plot("csv_files/{}.csv".format(trial.get_names()[0]), exit_time)
    p.plot()


if __name__ == "__main__":
    main()
